import 'package:gitlab_flutter/providers/package_info.dart';
import 'package:gitlab_flutter/providers/user.dart';
import 'package:gitlab_flutter/ui/home_nav.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    final userProvider = Provider.of<UserProvider>(context);
    if (userProvider.loading) {
      return _buildLoading();
    }
    if (userProvider.user == null) {
      return _buildWelcome(context);
    }
    return const HomeNav();
  }

  Widget _buildLoading() =>
      const Scaffold(body: Center(child: CircularProgressIndicator()));

  Widget _buildWelcome(BuildContext context) {
    final provider = Provider.of<PackageInfoProvider>(context);
    return Scaffold(
        body: Container(
      color: Theme.of(context).primaryColor,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 100),
              child: Text(
                provider.packageInfo.appName,
                style: TextStyle(
                    color: Theme.of(context).colorScheme.secondary,
                    fontStyle: FontStyle.italic,
                    fontWeight: FontWeight.bold,
                    fontSize: 36,
                    shadows: [
                      Shadow(
                          offset: const Offset(0, 5),
                          color: Theme.of(context).colorScheme.secondary,
                          blurRadius: 20)
                    ]),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 100),
              child: Column(children: <Widget>[
                const Text('👇', style: TextStyle(fontSize: 50)),
                OutlinedButton(
                  onPressed: () => _navigateToConfig(context),
                  child: const Text('Config Access_Token & Host'),
                ),
              ]),
            )
          ],
        ),
      ),
    ));
  }

  void _navigateToConfig(BuildContext context) {
    Navigator.pushNamed(context, '/config');
  }
}
