import 'package:gitlab_flutter/model/todo.dart';
import 'package:gitlab_flutter/util/widget_util.dart';
import 'package:gitlab_flutter/widget/comm_listview.dart';
import 'package:flutter/material.dart';

class TabTodo extends CommListWidget {
  const TabTodo({super.key});

  @override
  State<StatefulWidget> createState() => TodoState();
}

class TodoState extends CommListState<TabTodo> {
  @override
  Widget childBuild(BuildContext context, int index) {
    final todoItem = Todo.fromJson(data[index]);
    return Card(
        child: ExpansionTile(
      leading: loadAvatar(todoItem.author?.avatarUrl, todoItem.author?.name),
      title: Text.rich(TextSpan(
          text: '${todoItem.author?.name} ',
          style: const TextStyle(fontWeight: FontWeight.w100),
          children: [
            TextSpan(
                text: todoItem.actionName.toUpperCase(),
                style: const TextStyle(fontWeight: FontWeight.bold)),
            TextSpan(
                text: ' ${todoItem.targetType} ',
                style: const TextStyle(fontWeight: FontWeight.w400)),
            TextSpan(text: todoItem.target?.title)
          ])),
      trailing: OutlinedButton(
        child: const Text('Done'),
        onPressed: () {},
      ),
      children: <Widget>[Text(todoItem.createdAt ?? 'Unknown date')],
    ));
  }

  @override
  String endPoint() => 'todos?state=pending';
}
