import 'package:gitlab_flutter/const.dart';
import 'package:gitlab_flutter/model/user.dart';
import 'package:gitlab_flutter/providers/package_info.dart';
import 'package:gitlab_flutter/providers/theme.dart';
import 'package:gitlab_flutter/providers/user.dart';
import 'package:gitlab_flutter/ui/group/groups_tab.dart';
import 'package:gitlab_flutter/ui/project/project_tabs.dart';
import 'package:gitlab_flutter/ui/todo/todo_tab.dart';
import 'package:gitlab_flutter/util/widget_util.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class HomeNav extends StatefulWidget {
  const HomeNav({super.key});

  @override
  State<StatefulWidget> createState() => _State();
}

class TabItem {
  final IconData icon;
  final String name;
  final WidgetBuilder builder;

  TabItem(this.icon, this.name, this.builder);
}

class _State extends State<HomeNav> {
  int _currentTab = 0;
  List<TabItem> _items = [];

  @override
  void initState() {
    super.initState();
    _items = [
      TabItem(Icons.category, 'Project', (_) => const TabProject()),
      TabItem(Icons.group, 'Groups', (_) => const TabGroups()),
      TabItem(Icons.group, 'To-Do List', (_) => const TabTodo()),
    ];
    _loadNavIndexFromLocal();
  }

  @override
  Widget build(BuildContext context) {
    final themeProvider = Provider.of<ThemeProvider>(context, listen: false);
    final userProvider = Provider.of<UserProvider>(context, listen: false);
    final user = userProvider.user;
    final tabs = _items.map<Widget>((item) => item.builder(context)).toList();
    return Scaffold(
      appBar: AppBar(title: Text(_items[_currentTab].name)),
      drawer: _buildNav(context, user, themeProvider, _items),
      body: Builder(builder: (context) {
        return IndexedStack(index: _currentTab, children: tabs);
      }),
    );
  }

  Drawer _buildNav(BuildContext context, User? user,
      ThemeProvider themeProvider, List<TabItem> items) {
    List<Widget> widgets = [];

    final header = UserAccountsDrawerHeader(
      decoration:
          BoxDecoration(color: Theme.of(context).scaffoldBackgroundColor),
      accountName: Text(
        user?.name ?? '',
        style: TextStyle(
          color: Theme.of(context).colorScheme.primary,
        ),
      ),
      accountEmail: Text(
        user?.email ?? '',
        style: TextStyle(
          color: Theme.of(context).colorScheme.secondary,
        ),
      ),
      currentAccountPicture: loadAvatar(user?.avatarUrl, user?.name),
    );
    final tabs = _buildTabNav(items, context);
    final config = ListTile(
      leading: const Icon(Icons.settings),
      title: const Text('Config'),
      onTap: () => _navigateToConfig(context),
    );

    final packageInfoProvider = Provider.of<PackageInfoProvider>(context);
    final about = AboutListTile(
      icon: const Icon(Icons.apps),
      applicationName: packageInfoProvider.packageInfo.appName,
      applicationVersion: packageInfoProvider.packageInfo.version,
      applicationLegalese: APP_LEGEND,
      applicationIcon: Image.network(
        APP_ICON_URL,
        width: 60,
        height: 60,
      ),
      aboutBoxChildren: <Widget>[
        OutlinedButton(
          child: const Text('FeedBack'),
          onPressed: () => launchUrl(Uri.parse(APP_FEED_BACK_URL)),
        ),
        OutlinedButton(
          child: const Text('See in GitHub'),
          onPressed: () => launchUrl(Uri.parse(APP_REPO_URL)),
        )
      ],
    );
    final footer = Padding(
      padding: const EdgeInsets.only(left: 20),
      child: Row(
        children: <Widget>[
          const Text('Dark Theme'),
          Switch(
            onChanged: (isDark) => _changeTheme(isDark, themeProvider),
            value: themeProvider.isDark,
          )
        ],
      ),
    );

    widgets.add(header);
    widgets.addAll(tabs);
    widgets.add(config);
    widgets.add(about);
    widgets.add(footer);
    return Drawer(child: ListView(children: widgets));
  }

  List<Widget> _buildTabNav(List<TabItem> items, BuildContext context) {
    return items.map<Widget>((item) {
      final index = items.indexOf(item);
      return ListTile(
        selected: _currentTab == index,
        leading: Icon(item.icon),
        title: Text(item.name),
        onTap: () => _switchTab(item, index, context),
      );
    }).toList();
  }

  void _changeTheme(bool isDark, ThemeProvider themeProvider) {
    if (isDark) {
      themeProvider.switchToDark();
    } else {
      themeProvider.switchToLight();
    }
  }

  void _switchTab(TabItem item, int index, BuildContext context) {
    Navigator.of(context).pop();
    setState(() => _currentTab = index);
    _saveNavIndexToLocal(index);
  }

  void _navigateToConfig(BuildContext context) {
    Navigator.pop(context);
    Navigator.pushNamed(context, '/config');
  }

  void _saveNavIndexToLocal(int tabIndex) async {
    final sp = await SharedPreferences.getInstance();
    sp.setInt(KEY_TAB_INDEX, tabIndex);
  }

  void _loadNavIndexFromLocal() {
    SharedPreferences.getInstance()
        .then((sp) => sp.getInt(KEY_TAB_INDEX) ?? 0)
        .then((index) {
      if (mounted) {
        setState(() => _currentTab = index);
      }
    });
  }
}
