import 'package:gitlab_flutter/model/merge_request.dart';
import 'package:gitlab_flutter/providers/theme.dart';
import 'package:gitlab_flutter/ui/project/mr/mr_home.dart';
import 'package:gitlab_flutter/util/widget_util.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MrListItem extends StatelessWidget {
  final MergeRequest mr;

  const MrListItem({super.key, required this.mr});

  @override
  Widget build(BuildContext context) {
    final themeProvider = Provider.of<ThemeProvider>(context);
    bool assigned = mr.assignee != null;
    bool hadDescription = mr.description.isNotEmpty;
    String branch = '${mr.sourceBranch} → ${mr.targetBranch}';
    String uName = '';
    String? avatarUrl;
    if (assigned) {
      uName = mr.assignee?.username ?? '';
      avatarUrl = mr.assignee?.avatarUrl;
    }
    var card = Card(
      elevation: 1.0,
      margin: const EdgeInsets.only(bottom: 5.0, left: 4.0, right: 4.0, top: 5.0),
      child: InkWell(
        onTap: () => _toMrDetail(context, mr),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ListTile(
              title: RichText(
                text: TextSpan(
                    text: '$uName ',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 24,
                        color: Theme.of(context).colorScheme.secondary),
                    children: [
                      TextSpan(
                          text: 'assigned to ',
                          style: TextStyle(
                              fontSize: 12,
                              color: themeProvider.isDark
                                  ? Colors.white
                                  : Colors.black,
                              fontWeight: FontWeight.normal),
                          children: <TextSpan>[
                            TextSpan(
                                text: '\'${mr.title}\'',
                                style: TextStyle(
                                    fontSize: 18,
                                    color: Theme.of(context).colorScheme.primary,
                                    fontWeight: FontWeight.normal,
                                    fontStyle: FontStyle.italic))
                          ])
                    ]),
              ),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[Chip(label: Text(branch)), Text(mr.webUrl)],
              ),
              leading: mr.mergeStatus == 'can_be_merged'
                  ? const Icon(
                      Icons.done,
                      color: Colors.green,
                    )
                  : const Icon(
                      Icons.highlight_off,
                      color: Colors.red,
                    ),
              trailing: assigned
                  ? loadAvatar(
                      avatarUrl,
                      uName,
                    )
                  : const IgnorePointer(),
            ),
            hadDescription
                ? Padding(
                    padding: const EdgeInsets.all(10),
                    child: Text(
                      mr.description,
                    ),
                  )
                : const IgnorePointer(),
            // MrApprove(mr.projectId, mr.iid)
          ],
        ),
      ),
    );
    return card;
  }

  _toMrDetail(BuildContext context, MergeRequest mr) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => PageMrDetail(mr.projectId, mr.iid)));
  }
}
