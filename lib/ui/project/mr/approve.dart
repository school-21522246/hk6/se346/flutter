import 'package:gitlab_flutter/api.dart';
import 'package:gitlab_flutter/model/approvals.dart';
import 'package:gitlab_flutter/user_helper.dart';
import 'package:flutter/material.dart';

class MrApprove extends StatefulWidget {
  final int projectId;
  final int mrIID;
  final bool showActions;

  const MrApprove(this.projectId, this.mrIID, {super.key, this.showActions = false});

  @override
  State<StatefulWidget> createState() => _MrApproveState();
}

class _MrApproveState extends State<MrApprove> {
  Approvals? approval;
  bool isApproving = false;

  void _loadApprove() async {
    final apiResp =
        await ApiService.mrApproveData(widget.projectId, widget.mrIID);
    if (mounted) {
      setState(() {
        approval = apiResp.data;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _loadApprove();
  }

  @override
  Widget build(BuildContext context) {
    if (approval == null || isApproving) {
      return const LinearProgressIndicator();
    }
    return _buildItem(approval);
  }

  Widget _buildItem(Approvals? ap) {
    int requireApproves = ap?.approvalsRequired ?? 0;
    int? hadApproval = ap?.approvedBy != null ? ap?.approvedBy.length : 0;
    bool allApproval = requireApproves == hadApproval;
    bool iHadApproval = ap?.approvedBy != null
        ? ap!.approvedBy.any((item) => item.user?.id == UserHelper.getUser()?.id)
        : false;

    return Card(
      child: Padding(
        padding: const EdgeInsets.all(5),
        child: Row(
          children: <Widget>[
            const Icon(Icons.sentiment_neutral),
            Expanded(
              child: Text(
                'Approvals: $hadApproval of $requireApproves',
                style:
                    TextStyle(color: allApproval ? Colors.green : Colors.grey),
              ),
            ),
            widget.showActions ? _buildActions(iHadApproval) : const IgnorePointer(),
          ],
        ),
      ),
    );
  }

  Widget _buildActions(bool iHadApproval) {
    return OutlinedButton(
      onPressed: () => _approveOrUnApprove(!iHadApproval),
      child: Text(iHadApproval ? 'UnApprove' : 'Approve'),
    );
  }

  void _approveOrUnApprove(bool isApprove) async {
    setState(() {
      isApproving = true;
    });
    final ApiResp apiData =
        await ApiService.approve(widget.projectId, widget.mrIID, isApprove);
    setState(() {
      isApproving = false;
    });
    if (apiData.success) {
      _loadApprove();
    } else {
      if (!mounted) return;
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('${apiData.err}'),
          backgroundColor: Colors.red,
        ),
      );
    }
  }
}
