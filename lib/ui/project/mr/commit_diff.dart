import 'package:gitlab_flutter/api.dart';
import 'package:gitlab_flutter/model/commit.dart';
import 'package:gitlab_flutter/model/diff.dart';
import 'package:gitlab_flutter/providers/theme.dart';
import 'package:gitlab_flutter/ui/project/mr/diff.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PageCommitDiff extends StatefulWidget {
  final int projectId;
  final Commit commit;

  const PageCommitDiff(this.projectId, this.commit, {super.key});

  @override
  State<StatefulWidget> createState() => _DiffState();
}

class _DiffState extends State<PageCommitDiff> {
  List<Diff> diffs = [];
  double _codeFontSize = 14.0;

  _loadDiffs() async {
    final resp =
        await ApiService.commitDiff(widget.projectId, widget.commit.id);
    if (resp.success) {
      if (mounted) {
        setState(() => diffs = resp.data ?? []);
      }
    }
  }

  @override
  void initState() {
    super.initState();
    _loadDiffs();
  }

  _buildFontControl() {
    return Row(
      children: <Widget>[
        Text(
          'Code Size: $_codeFontSize',
        ),
        OutlinedButton(
          child: const Text('-'),
          onPressed: () => setState(() => _codeFontSize -= 1),
        ),
        OutlinedButton(
          child: const Text('+'),
          onPressed: () => setState(() => _codeFontSize += 1),
        )
      ],
    );
  }

  _buildDiff(Diff item, Color normalColor) {
    return Card(
        child: ExpansionTile(
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                item.newFile
                    ? const IgnorePointer()
                    : Text(
                        item.oldPath,
                        style: const TextStyle(color: Colors.red),
                      ),
                item.deletedFile
                    ? const IgnorePointer()
                    : Text(
                        item.newPath,
                        style: const TextStyle(color: Colors.green),
                      ),
              ],
            ),
            children: [
              _buildFontControl(),
              const Divider(color: Colors.grey),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: diffToText(item.diff, Colors.red, Colors.green, normalColor,
                      fontSize: _codeFontSize),
                ),
              ),
            ]
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    final themeProvider = Provider.of<ThemeProvider>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.commit.title),
      ),
      body: diffs.isEmpty
          ? const Center(child: CircularProgressIndicator())
          : ListView.builder(
              itemCount: diffs.length,
              itemBuilder: (context, index) {
                final item = diffs[index];
                return _buildDiff(item, themeProvider.isDark ? Colors.white : Colors.black);
              }),
    );
  }
}
