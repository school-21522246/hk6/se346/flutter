import 'package:gitlab_flutter/ui/project/jobs/jobs_tab.dart';
import 'package:gitlab_flutter/ui/project/commits/commits_tab.dart';
import 'package:gitlab_flutter/ui/project/mr/mr_list.dart';
import 'package:flutter/material.dart';

class PageProjectDetail extends StatelessWidget {
  final String projectName;
  final int projectId;

  const PageProjectDetail(this.projectName, this.projectId, {super.key});

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
            title: Text(projectName),
            centerTitle: false,
            bottom: const TabBar(
              tabs: <Widget>[
                Tab(text: 'Merge Requests'),
                Tab(text: 'Commits'),
                Tab(text: 'Jobs'),
              ],
            )),
        body: TabBarView(
          children: [MRTab(projectId), CommitTab(projectId), JobsTab(projectId)],
        ),
      ),
    );
  }
}
