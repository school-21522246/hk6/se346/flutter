import 'package:gitlab_flutter/api.dart';
import 'package:gitlab_flutter/model/commit.dart';
import 'package:gitlab_flutter/widget/comm_listview.dart';
import 'package:flutter/material.dart';

class CommitTab extends CommListWidget {
  final int projectId;

  const CommitTab(this.projectId, {super.key});

  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends CommListState<CommitTab> {
  @override
  Widget childBuild(BuildContext context, int index) {
    return JobWidget(commit: Commit.fromJson(data[index]));
  }

  @override
  String endPoint() => ApiEndPoint.commits(widget.projectId);
}

class JobWidget extends StatelessWidget {
  final Commit commit;

  const JobWidget({super.key, required this.commit});

  @override
  Widget build(BuildContext context) {
    return Card(child: Padding(padding: const EdgeInsets.all(8), child: _item(commit)));
  }

  Widget _item(Commit commit) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(commit.title,
            style: const TextStyle(fontWeight: FontWeight.bold)),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text('Author: ${commit.authorName}'),
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(3),
                  child: Text(commit.authorEmail),
                ),
              ],
            )
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(commit.shortId),
            Text(commit.createdAt.toString()),
          ],
        ),
      ],
    );
  }
}
