import 'package:gitlab_flutter/api.dart';
import 'package:gitlab_flutter/model/jobs.dart';
import 'package:gitlab_flutter/widget/comm_listview.dart';
import 'package:flutter/material.dart';

class JobsTab extends CommListWidget {
  final int projectId;

  const JobsTab(this.projectId, {super.key});

  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends CommListState<JobsTab> {
  @override
  Widget childBuild(BuildContext context, int index) {
    return JobWidget(job: Jobs.fromJson(data[index]));
  }

  @override
  String endPoint() => ApiEndPoint.projectJobs(widget.projectId);
}

class JobWidget extends StatelessWidget {
  final Jobs job;

  const JobWidget({super.key, required this.job});

  @override
  Widget build(BuildContext context) {
    return Card(child: Padding(padding: const EdgeInsets.all(8), child: _item(job)));
  }

  Widget _item(Jobs job) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(job.commit?.title ?? '',
            style: const TextStyle(fontWeight: FontWeight.bold)),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text('#${job.id} ${job.ref}'),
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(3),
                  child: Text(job.status),
                ),
                statusIcons.containsKey(job.status)
                    ? statusIcons[job.status]
                    : const Icon(Icons.error, size: 18)
              ],
            )
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(job.stage),
            Text(job.name),
          ],
        ),
      ],
    );
  }

  static get statusIcons => {
        'success': const Icon(Icons.check_circle, color: Colors.green, size: 18),
        'manual': const Icon(Icons.build, size: 18),
        'failed': const Icon(Icons.error_outline, color: Colors.redAccent, size: 18),
        'skipped': const Icon(Icons.skip_next, size: 18),
        'canceled': const Icon(Icons.cancel, size: 18)
      };
}
