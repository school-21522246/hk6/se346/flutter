import 'package:gitlab_flutter/model/group.dart';
import 'package:gitlab_flutter/widget/comm_listview.dart';
import 'package:flutter/material.dart';

class TabGroups extends CommListWidget {
  const TabGroups({super.key}) : super(canPullUp: false, withPage: false);

  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends CommListState<TabGroups> {
  @override
  Widget build(BuildContext context) {
    return data.isEmpty
        ? GridView.builder(
            itemCount: data.length,
            gridDelegate:
                const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
            itemBuilder: (context, index) {
              return childBuild(context, index);
            }).build(context)
        : super.build(context);
  }

  @override
  Widget childBuild(BuildContext context, int index) {
    final item = data[index];
    return _buildItem(item);
  }

  Widget _buildItem(item) {
    final group = Group.fromJson(item);
    return Card(
      child: InkWell(
        onTap: () {
          ScaffoldMessenger.of(context).hideCurrentSnackBar();
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text('${group.name}: ${group.description}'),
              backgroundColor: Theme.of(context).primaryColor,
            ),
          );
        },
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: CircleAvatar(
              radius: 40,
              child: Text(
                group.name,
                textAlign: TextAlign.center,
                overflow: TextOverflow.fade,
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  String endPoint() => 'groups';
}
