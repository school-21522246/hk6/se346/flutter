import 'package:gitlab_flutter/const.dart';
import 'package:gitlab_flutter/gitlab_client.dart';
import 'package:gitlab_flutter/providers/user.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ConfigPage extends StatefulWidget {
  const ConfigPage({super.key});

  @override
  State<StatefulWidget> createState() => _ConfigState();
}

class _ConfigState extends State<ConfigPage> {
  String? _token, _host, _version;
  late UserProvider userProvider;

  @override
  void initState() {
    super.initState();
    _loadConfig();
  }

  @override
  Widget build(BuildContext context) {
    userProvider = Provider.of<UserProvider>(context);
    if (userProvider.testSuccess) {
      userProvider.resetTestState();
      Future.delayed(
          const Duration(milliseconds: 300), () => Navigator.pop(context));
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('Config'),
        elevation: 0,
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: Builder(
        builder: (context) {
          return Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  TextField(
                    decoration: InputDecoration(
                        hintText: _token ?? 'Access Token:',
                        helperText:
                            'You can create personal access token from your GitLab profile.'),
                    textInputAction: TextInputAction.next,
                    keyboardType: TextInputType.text,
                    onChanged: (token) => _token = token,
                  ),
                  TextField(
                    decoration: InputDecoration(
                        hintText: _host ?? 'GitLab Host:',
                        helperText: 'GitLab Host, default https://gitlab.com'),
                    textInputAction: TextInputAction.done,
                    keyboardType: TextInputType.url,
                    onChanged: (host) => _host = host,
                  ),
                  TextField(
                    decoration: InputDecoration(
                        hintText: _version ?? 'Your gitlab api version',
                        helperText: 'Api version, default v4'),
                    textInputAction: TextInputAction.done,
                    keyboardType: TextInputType.text,
                    maxLines: 1,
                    onChanged: (v) => _version = v,
                  ),
                  userProvider.testErr != null
                      ? Text(userProvider.testErr ?? '',
                          style: const TextStyle(color: Colors.red))
                      : const IgnorePointer(ignoring: true),
                  userProvider.testing
                      ? const Column(
                          children: <Widget>[
                            CircularProgressIndicator(),
                            Text('Test connection')
                          ],
                        )
                      : const IgnorePointer(ignoring: true),
                ],
              ));
        },
      ),
      bottomSheet: BottomSheet(
          onClosing: () {},
          builder: (context) {
            return Padding(
                padding: const EdgeInsets.only(bottom: 50),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 2,
                      child: OutlinedButton(
                        child: const Text('Test & Save'),
                        onPressed: () {
                          if (_token == null) {
                            return;
                          }
                          _testConfig(context);
                        },
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: OutlinedButton(
                        child: const Text('Reset'),
                        onPressed: () => _reset(),
                      ),
                    ),
                  ],
                ));
          }),
    );
  }

  void _testConfig(BuildContext context) {
    userProvider.testConfig(_host, _token!, _version);
  }

  void _loadConfig() async {
    final SharedPreferences sp = await SharedPreferences.getInstance();
    setState(() {
      _token = sp.getString(KEY_ACCESS_TOKEN);
      _host = sp.getString(KEY_HOST) ?? DEFAULT_API_HOST;
      _version = sp.getString(KEY_API_VERSION) ?? DEFAULT_API_VERSION;
    });
  }

  void _reset() {
    Navigator.pop(context);
    userProvider.logOut();
  }
}
