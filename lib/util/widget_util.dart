import 'package:flutter/material.dart';

Widget loadAvatar(String? url, String? name, {Color color = Colors.teal}) {
  assert(name != null);
  if (url != null) {
    NetworkImage image;
    image = NetworkImage(url);
    return CircleAvatar(
      backgroundImage: image,
      backgroundColor: color,
    );
  }
  return CircleAvatar(
    backgroundColor: color,
    child: Text(
      name ?? '',
      textAlign: TextAlign.center,
      overflow: TextOverflow.ellipsis,
      softWrap: true,
    ),
  );
}
