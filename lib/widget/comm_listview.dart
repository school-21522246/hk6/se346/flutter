import 'package:gitlab_flutter/gitlab_client.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

/// [canPullUp] This bool will affect whether or not to have the function of drop-up load
/// [canPullDown] This bool will affect whether or not to have the function of drop-down refresh
/// [withPage] This bool will affect whether or not to add page arg to request url
abstract class CommListWidget extends StatefulWidget {
  final bool canPullUp;

  final bool canPullDown;

  final bool withPage;

  const CommListWidget(
      {super.key, this.canPullDown = true, this.canPullUp = true, this.withPage = true});
}

abstract class CommListState<T extends CommListWidget> extends State<T>
    with AutomaticKeepAliveClientMixin {
  List<dynamic> data = [];
  late int page;
  late int total;
  late int next;

  final RefreshController _refreshController = RefreshController();

  /// eg: merge_request?status=open
  String? endPoint();

  loadData({nextPage = 1}) async {
    Dio dio = GitlabClient.buildDio();

    String url;
    final endpoint = endPoint() ?? '';
    if (widget.withPage) {
      if (!endpoint.contains('?')) {
        url = '$endpoint?page=$nextPage&per_page=10';
      } else {
        url = '$endpoint&page=$nextPage&per_page=10';
      }
    } else {
      url = endpoint;
    }
    print('url: $url');

    final remoteData = await dio
        .get<dynamic>(url)
        .then((resp) {
          if (resp.headers['x-page'] != null) {
            page = int.tryParse(resp.headers['x-page']![0]) ?? 0;
          }
          if (resp.headers['x-next-page'] != null) {
            next = int.tryParse(resp.headers['x-next-page']![0]) ?? 0;
          }
          if (resp.headers['x-total-pages'] != null) {
            total = int.tryParse(resp.headers['x-total-pages']![0]) ?? 0;
          } else {
            total = next != 0 ? next : page;
          }
          return resp;
        })
        .then((resp) => resp.data)
        .catchError((err) {
          return [];
        });
    
    return Future(() {
      final List<dynamic> remote = [];
      remoteData.forEach((item) {
        if (!itemShouldRemove(item)) {
          remote.add(item);
        }
      });
      return remote;
    });
  }

  _loadMore() async {
    if (page == total) {
      _refreshController.loadNoData();
    } else {
      final remoteData = await loadData(nextPage: next);
      if (mounted) {
        setState(() {
          data.addAll(remoteData);
        });
        if (page == total) {
          _refreshController.loadNoData();
        } else {
          _refreshController.loadComplete();
        }
      }
    }
  }

  _loadNew() async {
    final remoteData = await loadData() as List;
    if (mounted) {
      setState(() {
        data = remoteData;
      });
      _refreshController.refreshCompleted();
    }
  }

  @override
  void initState() {
    super.initState();
    _loadNew();
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  Widget childBuild(BuildContext context, int index);

  bool itemShouldRemove(dynamic item) => false;

  Widget buildEmptyView() {
    return Center(
      child: Container(
        margin: const EdgeInsets.only(top: 100),
        child: const Text.rich(
          TextSpan(
            text: '🎉 No More 🎉\n\nPull Down To Refresh',
            style: TextStyle(fontSize: 16),
          ),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  Widget buildDataListView() {
    return SmartRefresher(
        controller: _refreshController,
        enablePullDown: widget.canPullDown,
        enablePullUp: widget.canPullUp,
        onRefresh: () => _loadNew(),
        onLoading: () => _loadMore(),
        child: data.isEmpty
            ? ListView.builder(
                itemCount: 1,
                itemBuilder: (ctx, index) {
                  return buildEmptyView();
                })
            : ListView.builder(
                itemCount: data.length,
                itemBuilder: (context, index) {
                  return childBuild(context, index);
                }));
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    // return data.isEmpty
    //     ? const Center(child: CircularProgressIndicator())
    //     : buildDataListView();
    return buildDataListView();
  }

  @override
  bool get wantKeepAlive => true;
}
