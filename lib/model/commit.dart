import 'package:gitlab_flutter/util/date_util.dart';

class Commit {
  late String id;
  late String shortId;
  late String title;
  late String authorName;
  late String authorEmail;
  late DateTime createdAt;
  late String message;

  Commit(
      {required this.id,
      required this.shortId,
      required this.title,
      required this.authorName,
      required this.authorEmail,
      required this.createdAt,
      required this.message});

  Commit.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    shortId = json['short_id'];
    title = json['title'];
    authorName = json['author_name'];
    authorEmail = json['author_email'];
    createdAt = string2Datetime(json['created_at']);
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['short_id'] = shortId;
    data['title'] = title;
    data['author_name'] = authorName;
    data['author_email'] = authorEmail;
    data['created_at'] = createdAt;
    data['message'] = message;
    return data;
  }
}
