class Todo {
  late int id;
  Project? project;
  Author? author;
  late String actionName;
  late String targetType;
  Target? target;
  late String targetUrl;
  late String body;
  late String state;
  late String? createdAt;

  Todo(
      {required this.id,
      this.project,
      this.author,
      required this.actionName,
      required this.targetType,
      this.target,
      required this.targetUrl,
      required this.body,
      required this.state,
      required this.createdAt});

  Todo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    project = json['project'] != null ? Project.fromJson(json['project']) : null;
    author = json['author'] != null ? Author.fromJson(json['author']) : null;
    actionName = json['action_name'];
    targetType = json['target_type'];
    target = json['target'] != null ? Target.fromJson(json['target']) : null;
    targetUrl = json['target_url'];
    body = json['body'];
    state = json['state'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    if (project != null) {
      data['project'] = project?.toJson();
    }
    if (author != null) {
      data['author'] = author?.toJson();
    }
    data['action_name'] = actionName;
    data['target_type'] = targetType;
    if (target != null) {
      data['target'] = target?.toJson();
    }
    data['target_url'] = targetUrl;
    data['body'] = body;
    data['state'] = state;
    if (createdAt != null) {
      data['created_at'] = createdAt;
    }
    return data;
  }
}

class Project {
  late int id;
  late String name;
  late String nameWithNamespace;
  late String path;
  late String pathWithNamespace;

  Project(
      {required this.id,
      required this.name,
      required this.nameWithNamespace,
      required this.path,
      required this.pathWithNamespace});

  Project.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    nameWithNamespace = json['name_with_namespace'];
    path = json['path'];
    pathWithNamespace = json['path_with_namespace'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['name_with_namespace'] = nameWithNamespace;
    data['path'] = path;
    data['path_with_namespace'] = pathWithNamespace;
    return data;
  }
}

class Author {
  late String name;
  late String username;
  late int id;
  late String state;
  late String avatarUrl;
  late String webUrl;

  Author(
      {required this.name,
      required this.username,
      required this.id,
      required this.state,
      required this.avatarUrl,
      required this.webUrl});

  Author.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    username = json['username'];
    id = json['id'];
    state = json['state'];
    avatarUrl = json['avatar_url'];
    webUrl = json['web_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['username'] = username;
    data['id'] = id;
    data['state'] = state;
    data['avatar_url'] = avatarUrl;
    data['web_url'] = webUrl;
    return data;
  }
}

class Target {
  late int id;
  late int iid;
  late int projectId;
  late String title;
  late String? description;
  late String? state;
  late String? createdAt;
  late String? updatedAt;
  late String? targetBranch;
  late String? sourceBranch;
  late int? upvotes;
  late int? downvotes;
  Author? author;
  Assignee? assignee;
  late int? sourceProjectId;
  late int? targetProjectId;
  late List<String> labels;
  late bool? workInProgress;
  Milestone? milestone;
  late bool? mergeWhenPipelineSucceeds;
  late String? mergeStatus;
  late bool? subscribed;
  late int? userNotesCount;

  Target(
      {required this.id,
      required this.iid,
      required this.projectId,
      required this.title,
      required this.description,
      required this.state,
      required this.createdAt,
      required this.updatedAt,
      required this.targetBranch,
      required this.sourceBranch,
      required this.upvotes,
      required this.downvotes,
      this.author,
      this.assignee,
      required this.sourceProjectId,
      required this.targetProjectId,
      required this.labels,
      required this.workInProgress,
      this.milestone,
      required this.mergeWhenPipelineSucceeds,
      required this.mergeStatus,
      required this.subscribed,
      required this.userNotesCount});

  Target.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    iid = json['iid'];
    projectId = json['project_id'];
    title = json['title'];
    description = json['description'];
    state = json['state'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    targetBranch = json['target_branch'];
    sourceBranch = json['source_branch'];
    upvotes = json['upvotes'];
    downvotes = json['downvotes'];
    author =
        json['author'] != null ? Author.fromJson(json['author']) : null;
    assignee =
        json['assignee'] != null ? Assignee.fromJson(json['assignee']) : null;
    sourceProjectId = json['source_project_id'];
    targetProjectId = json['target_project_id'];
    labels = json['labels'].cast<String>();
    workInProgress = json['work_in_progress'];
    milestone =
        json['milestone'] != null ? Milestone.fromJson(json['milestone']) : null;
    mergeWhenPipelineSucceeds = json['merge_when_pipeline_succeeds'];
    mergeStatus = json['merge_status'];
    subscribed = json['subscribed'];
    userNotesCount = json['user_notes_count'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['iid'] = iid;
    data['project_id'] = projectId;
    data['title'] = title;
    data['description'] = description;
    data['state'] = state;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['target_branch'] = targetBranch;
    data['source_branch'] = sourceBranch;
    data['upvotes'] = upvotes;
    data['downvotes'] = downvotes;
    if (author != null) {
      data['author'] = author?.toJson();
    }
    if (assignee != null) {
      data['assignee'] = assignee?.toJson();
    }
    data['source_project_id'] = sourceProjectId;
    data['target_project_id'] = targetProjectId;
    data['labels'] = labels;
    data['work_in_progress'] = workInProgress;
    if (milestone != null) {
      data['milestone'] = milestone?.toJson();
    }
    data['merge_when_pipeline_succeeds'] = mergeWhenPipelineSucceeds;
    data['merge_status'] = mergeStatus;
    data['subscribed'] = subscribed;
    data['user_notes_count'] = userNotesCount;
    return data;
  }
}

class Assignee {
  late String name;
  late String username;
  late int id;
  late String state;
  late String avatarUrl;
  late String webUrl;

  Assignee(
      {required this.name,
      required this.username,
      required this.id,
      required this.state,
      required this.avatarUrl,
      required this.webUrl});

  Assignee.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    username = json['username'];
    id = json['id'];
    state = json['state'];
    avatarUrl = json['avatar_url'];
    webUrl = json['web_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['username'] = username;
    data['id'] = id;
    data['state'] = state;
    data['avatar_url'] = avatarUrl;
    data['web_url'] = webUrl;
    return data;
  }
}

class Milestone {
  late int id;
  late int iid;
  late int projectId;
  late String title;
  late String description;
  late String state;
  late String createdAt;
  late String updatedAt;
  late String dueDate;

  Milestone(
      {required this.id,
      required this.iid,
      required this.projectId,
      required this.title,
      required this.description,
      required this.state,
      required this.createdAt,
      required this.updatedAt,
      required this.dueDate});

  Milestone.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    iid = json['iid'];
    projectId = json['project_id'];
    title = json['title'];
    description = json['description'];
    state = json['state'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    dueDate = json['due_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['iid'] = iid;
    data['project_id'] = projectId;
    data['title'] = title;
    data['description'] = description;
    data['state'] = state;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['due_date'] = dueDate;
    return data;
  }
}
