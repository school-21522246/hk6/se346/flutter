class Approvals {
  late int id;
  late int iid;
  late int projectId;
  late String title;
  late String description;
  late String state;
  late String createdAt;
  late String updatedAt;
  late String mergeStatus;
  late int approvalsRequired;
  late int approvalsLeft;
  late List<ApprovedBy> approvedBy = [];

  Approvals(
      {required this.id,
      required this.iid,
      required this.projectId,
      required this.title,
      required this.description,
      required this.state,
      required this.createdAt,
      required this.updatedAt,
      required this.mergeStatus,
      required this.approvalsRequired,
      required this.approvalsLeft,
      required this.approvedBy});

  Approvals.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    iid = json['iid'];
    projectId = json['project_id'];
    title = json['title'];
    description = json['description'];
    state = json['state'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    mergeStatus = json['merge_status'];
    approvalsRequired = json['approvals_required'];
    approvalsLeft = json['approvals_left'];
    if (json['approved_by'] != null) {
      approvedBy = [];
      json['approved_by'].forEach((v) {
        approvedBy.add(ApprovedBy.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['iid'] = iid;
    data['project_id'] = projectId;
    data['title'] = title;
    data['description'] = description;
    data['state'] = state;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['merge_status'] = mergeStatus;
    data['approvals_required'] = approvalsRequired;
    data['approvals_left'] = approvalsLeft;
    if (approvedBy.isNotEmpty) {
      data['approved_by'] = approvedBy.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ApprovedBy {
  User? user;

  ApprovedBy({this.user});

  ApprovedBy.fromJson(Map<String, dynamic> json) {
    user = json['user'] != null ? User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (user != null) {
      data['user'] = user!.toJson();
    }
    return data;
  }
}

class User {
  late String name;
  late String username;
  late int id;
  late String state;
  late String avatarUrl;
  late String webUrl;

  User(
      {required this.name,
      required this.username,
      required this.id,
      required this.state,
      required this.avatarUrl,
      required this.webUrl});

  User.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    username = json['username'];
    id = json['id'];
    state = json['state'];
    avatarUrl = json['avatar_url'];
    webUrl = json['web_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['username'] = username;
    data['id'] = id;
    data['state'] = state;
    data['avatar_url'] = avatarUrl;
    data['web_url'] = webUrl;
    return data;
  }
}
