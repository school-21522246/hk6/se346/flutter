class Discussion {
  late String id;
  late bool individualNote;
  late List<Notes> notes = [];

  Discussion(
      {required this.id, required this.individualNote, required this.notes});

  Discussion.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    individualNote = json['individual_note'];
    if (json['notes'] != null) {
      notes = [];
      json['notes'].forEach((v) {
        notes.add(Notes.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['individual_note'] = individualNote;
    data['notes'] = notes.map((v) => v.toJson()).toList();
    return data;
  }
}

class Notes {
  late int id;
  late String type;
  late String body;
  late String attachment;
  Author? author;
  late String createdAt;
  late String updatedAt;
  late bool system;
  late int noteableId;
  late String noteableType;
  late int noteableIid;
  late bool resolved;
  late bool resolvable;
  Author? resolvedBy;

  Notes(
      {required this.id,
      required this.type,
      required this.body,
      required this.attachment,
      this.author,
      required this.createdAt,
      required this.updatedAt,
      required this.system,
      required this.noteableId,
      required this.noteableType,
      required this.noteableIid,
      required this.resolved,
      required this.resolvable,
      this.resolvedBy});

  Notes.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    type = json['type'];
    body = json['body'];
    attachment = json['attachment'];
    author =
        json['author'] != null ? Author.fromJson(json['author']) : null;
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    system = json['system'];
    noteableId = json['noteable_id'];
    noteableType = json['noteable_type'];
    noteableIid = json['noteable_iid'];
    resolved = json['resolved'] ?? false;
    resolvable = json['resolvable'];
    resolvedBy = json['resolved_by'] != null
        ? Author.fromJson(json['author'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['type'] = type;
    data['body'] = body;
    data['attachment'] = attachment;
    if (author != null) {
      data['author'] = author?.toJson();
    }
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['system'] = system;
    data['noteable_id'] = noteableId;
    data['noteable_type'] = noteableType;
    data['noteable_iid'] = noteableIid;
    data['resolved'] = resolved;
    data['resolvable'] = resolvable;
    data['resolved_by'] = resolvedBy;
    return data;
  }
}

class Author {
  late int id;
  late String name;
  late String username;
  late String state;
  late String avatarUrl;
  late String webUrl;

  Author(
      {required this.id,
      required this.name,
      required this.username,
      required this.state,
      required this.avatarUrl,
      required this.webUrl});

  Author.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    username = json['username'];
    state = json['state'];
    avatarUrl = json['avatar_url'];
    webUrl = json['web_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['username'] = username;
    data['state'] = state;
    data['avatar_url'] = avatarUrl;
    data['web_url'] = webUrl;
    return data;
  }
}
