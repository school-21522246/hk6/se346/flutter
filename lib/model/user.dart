class User {
  late int id;
  late String username;
  late String? email;
  late String name;
  late String state;
  late String avatarUrl;
  late String webUrl;
  late String createdAt;
  late String bio;
  late String location;
  late String publicEmail;
  late String skype;
  late String linkedin;
  late String twitter;
  late String websiteUrl;
  late String organization;
  late String? lastSignInAt;
  late String? confirmedAt;
  late int themeId;
  late String? lastActivityOn;
  late int colorSchemeId;
  late int projectsLimit;
  late String currentSignInAt;
  List<Identities> identities = [];
  late bool canCreateGroup;
  late bool canCreateProject;
  late bool twoFactorEnabled;
  late bool external;
  late bool privateProfile;

  User(
      {required this.id,
      required this.username,
      required this.email,
      required this.name,
      required this.state,
      required this.avatarUrl,
      required this.webUrl,
      required this.createdAt,
      required this.bio,
      required this.location,
      required this.publicEmail,
      required this.skype,
      required this.linkedin,
      required this.twitter,
      required this.websiteUrl,
      required this.organization,
      required this.lastSignInAt,
      required this.confirmedAt,
      required this.themeId,
      required this.lastActivityOn,
      required this.colorSchemeId,
      required this.projectsLimit,
      required this.currentSignInAt,
      required this.identities,
      required this.canCreateGroup,
      required this.canCreateProject,
      required this.twoFactorEnabled,
      required this.external,
      required this.privateProfile});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
    email = json['email'];
    name = json['name'];
    state = json['state'];
    avatarUrl = json['avatar_url'];
    webUrl = json['web_url'];
    createdAt = json['created_at'];
    bio = json['bio'];
    location = json['location'];
    publicEmail = json['public_email'];
    skype = json['skype'];
    linkedin = json['linkedin'];
    twitter = json['twitter'];
    websiteUrl = json['website_url'];
    organization = json['organization'];
    lastSignInAt = json['last_sign_in_at'];
    confirmedAt = json['confirmed_at'];
    themeId = json['theme_id'];
    lastActivityOn = json['last_activity_on'];
    colorSchemeId = json['color_scheme_id'];
    projectsLimit = json['projects_limit'];
    currentSignInAt = json['current_sign_in_at'];
    if (json['identities'] != null) {
      identities = [];
      json['identities'].forEach((v) {
        identities.add(Identities.fromJson(v));
      });
    }
    canCreateGroup = json['can_create_group'];
    canCreateProject = json['can_create_project'];
    twoFactorEnabled = json['two_factor_enabled'];
    external = json['external'];
    privateProfile = json['private_profile'];
  }

  User.fromJsonInJobs(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    username = json['username'];
    state = json['state'];
    avatarUrl = json['avatar_url'];
    webUrl = json['web_url'];
    createdAt = json['created_at'];
    bio = json['bio'];
    location = json['location'];
    publicEmail = json['public_email'];
    skype = json['skype'];
    linkedin = json['linkedin'];
    twitter = json['twitter'];
    websiteUrl = json['website_url'];
    organization = json['organization'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['username'] = username;
    data['email'] = email;
    data['name'] = name;
    data['state'] = state;
    data['avatar_url'] = avatarUrl;
    data['web_url'] = webUrl;
    data['created_at'] = createdAt;
    data['bio'] = bio;
    data['location'] = location;
    data['public_email'] = publicEmail;
    data['skype'] = skype;
    data['linkedin'] = linkedin;
    data['twitter'] = twitter;
    data['website_url'] = websiteUrl;
    data['organization'] = organization;
    data['last_sign_in_at'] = lastSignInAt;
    data['confirmed_at'] = confirmedAt;
    data['theme_id'] = themeId;
    data['last_activity_on'] = lastActivityOn;
    data['color_scheme_id'] = colorSchemeId;
    data['projects_limit'] = projectsLimit;
    data['current_sign_in_at'] = currentSignInAt;
    if (identities.isNotEmpty) {
      data['identities'] = identities.map((v) => v.toJson()).toList();
    }
    data['can_create_group'] = canCreateGroup;
    data['can_create_project'] = canCreateProject;
    data['two_factor_enabled'] = twoFactorEnabled;
    data['external'] = external;
    data['private_profile'] = privateProfile;
    return data;
  }

  Map<String, dynamic> toJsonInJobs() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['username'] = username;
    data['state'] = state;
    data['avatar_url'] = avatarUrl;
    data['web_url'] = webUrl;
    data['created_at'] = createdAt;
    data['bio'] = bio;
    data['location'] = location;
    data['public_email'] = publicEmail;
    data['skype'] = skype;
    data['linkedin'] = linkedin;
    data['twitter'] = twitter;
    data['website_url'] = websiteUrl;
    data['organization'] = organization;
    return data;
  }

  @override
  String toString() {
    return 'User{id: $id, username: $username, name: $name}';
  }
}

class Identities {
  late String provider;
  late String externUid;

  Identities({required this.provider, required this.externUid});

  Identities.fromJson(Map<String, dynamic> json) {
    provider = json['provider'];
    externUid = json['extern_uid'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['provider'] = provider;
    data['extern_uid'] = externUid;
    return data;
  }
}
