import 'package:gitlab_flutter/model/user.dart';

class Pipeline {
  late int id;
  late String? sha;
  late String? ref;
  late String? status;
  late String? webUrl;
  late String? beforeSha;
  late bool? tag;
  late String? yamlErrors;
  late User? user;
  late String? createdAt;
  late String? updatedAt;
  late String? startedAt;
  late String? finishedAt;
  late String? committedAt;
  late int? duration;
  late String? coverage;

  Pipeline(
      {required this.id,
      required this.sha,
      required this.ref,
      required this.status,
      required this.webUrl,
      required this.beforeSha,
      required this.tag,
      required this.yamlErrors,
      this.user,
      required this.createdAt,
      required this.updatedAt,
      required this.startedAt,
      required this.finishedAt,
      required this.committedAt,
      required this.duration,
      required this.coverage});

  Pipeline.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    sha = json['sha'];
    ref = json['ref'];
    status = json['status'];
    webUrl = json['web_url'];
    beforeSha = json['before_sha'];
    tag = json['tag'];
    yamlErrors = json['yaml_errors'];
    user = json['user'] != null ? User.fromJson(json['user']) : null;
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    startedAt = json['started_at'];
    finishedAt = json['finished_at'];
    committedAt = json['committed_at'];
    duration = json['duration'];
    coverage = json['coverage'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['sha'] = sha;
    data['ref'] = ref;
    data['status'] = status;
    data['web_url'] = webUrl;
    data['before_sha'] = beforeSha;
    data['tag'] = tag;
    data['yaml_errors'] = yamlErrors;
    if (user != null) {
      data['user'] = user?.toJson();
    }
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['started_at'] = startedAt;
    data['finished_at'] = finishedAt;
    data['committed_at'] = committedAt;
    data['duration'] = duration;
    data['coverage'] = coverage;
    return data;
  }
}
