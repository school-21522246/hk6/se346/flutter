class Project {
  late int id;
  late String? description;
  late String defaultBranch;
  late String sshUrlToRepo;
  late String httpUrlToRepo;
  late String webUrl;
  late String? readmeUrl;
  late List<String> tagList;
  late String name;
  late String nameWithNamespace;
  late String path;
  late String pathWithNamespace;
  late String createdAt;
  late String lastActivityAt;
  late int forksCount;
  late String? avatarUrl;
  late int starCount;

  Project(
      {required this.id,
      required this.description,
      required this.defaultBranch,
      required this.sshUrlToRepo,
      required this.httpUrlToRepo,
      required this.webUrl,
      required this.readmeUrl,
      required this.tagList,
      required this.name,
      required this.nameWithNamespace,
      required this.path,
      required this.pathWithNamespace,
      required this.createdAt,
      required this.lastActivityAt,
      required this.forksCount,
      required this.avatarUrl,
      required this.starCount});

  Project.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    description = json['description'];
    defaultBranch = json['default_branch'];
    sshUrlToRepo = json['ssh_url_to_repo'];
    httpUrlToRepo = json['http_url_to_repo'];
    webUrl = json['web_url'];
    readmeUrl = json['readme_url'];
    tagList = json['tag_list'].cast<String>();
    name = json['name'];
    nameWithNamespace = json['name_with_namespace'];
    path = json['path'];
    pathWithNamespace = json['path_with_namespace'];
    createdAt = json['created_at'];
    lastActivityAt = json['last_activity_at'];
    forksCount = json['forks_count'];
    avatarUrl = json['avatar_url'];
    starCount = json['star_count'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['description'] = description;
    data['default_branch'] = defaultBranch;
    data['ssh_url_to_repo'] = sshUrlToRepo;
    data['http_url_to_repo'] = httpUrlToRepo;
    data['web_url'] = webUrl;
    data['readme_url'] = readmeUrl;
    data['tag_list'] = tagList;
    data['name'] = name;
    data['name_with_namespace'] = nameWithNamespace;
    data['path'] = path;
    data['path_with_namespace'] = pathWithNamespace;
    data['created_at'] = createdAt;
    data['last_activity_at'] = lastActivityAt;
    data['forks_count'] = forksCount;
    data['avatar_url'] = avatarUrl;
    data['star_count'] = starCount;
    return data;
  }
}
