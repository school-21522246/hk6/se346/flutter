import 'package:gitlab_flutter/model/pipeline.dart';
import 'package:gitlab_flutter/model/runner.dart';
import 'package:gitlab_flutter/model/user.dart';
import 'package:gitlab_flutter/util/date_util.dart';

class Jobs {
  late Commit? commit;
  late String? coverage;
  late DateTime createdAt;
  late String? startedAt;
  late String? finishedAt;
  late double? duration;
  late String? artifactsExpireAt;
  late int id;
  late String name;
  late Pipeline? pipeline;
  late String ref;
  late List<String> artifacts;
  late Runner? runner;
  late String stage;
  late String status;
  late bool tag;
  late String webUrl;
  late User? user;

  Jobs(
      {required this.commit,
      required this.coverage,
      required this.createdAt,
      required this.startedAt,
      required this.finishedAt,
      required this.duration,
      required this.artifactsExpireAt,
      required this.id,
      required this.name,
      required this.pipeline,
      required this.ref,
      required this.artifacts,
      required this.runner,
      required this.stage,
      required this.status,
      required this.tag,
      required this.webUrl,
      required this.user});

  Jobs.fromJson(Map<String, dynamic> json) {
    commit =
        json['commit'] != null ? Commit.fromJson(json['commit']) : null;
    coverage = json['coverage'];
    createdAt = string2Datetime(json['created_at']);
    startedAt = json['started_at'];
    finishedAt = json['finished_at'];
    duration = json['duration'];
    artifactsExpireAt = json['artifacts_expire_at'];
    id = json['id'];
    name = json['name'];
    pipeline = json['pipeline'] != null
        ? Pipeline.fromJson(json['pipeline'])
        : null;
    ref = json['ref'];
    artifacts = json['artifacts'].cast<String>();
    runner = json['runner'] != null
        ? Runner.fromJson(json['runner'])
        : null;
    stage = json['stage'];
    status = json['status'];
    tag = json['tag'];
    webUrl = json['web_url'];
    user = json['user'] != null ? User.fromJsonInJobs(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (commit != null) {
      data['commit'] = commit?.toJson();
    }
    data['coverage'] = coverage;
    data['created_at'] = createdAt;
    data['started_at'] = startedAt;
    data['finished_at'] = finishedAt;
    data['duration'] = duration;
    data['artifacts_expire_at'] = artifactsExpireAt;
    data['id'] = id;
    data['name'] = name;
    if (pipeline != null) {
      data['pipeline'] = pipeline?.toJson();
    }
    data['ref'] = ref;
    data['artifacts'] = artifacts;
    data['runner'] = runner?.toJson();
    data['stage'] = stage;
    data['status'] = status;
    data['tag'] = tag;
    data['web_url'] = webUrl;
    if (user != null) {
      data['user'] = user?.toJsonInJobs();
    }
    return data;
  }
}

class Commit {
  late String id;
  late String authorEmail;
  late String authorName;
  late String createdAt;
  late String message;
  late String shortId;
  late String title;

  Commit(
      {required this.authorEmail,
      required this.authorName,
      required this.createdAt,
      required this.id,
      required this.message,
      required this.shortId,
      required this.title});

  Commit.fromJson(Map<String, dynamic> json) {
    authorEmail = json['author_email'];
    authorName = json['author_name'];
    createdAt = json['created_at'];
    id = json['id'];
    message = json['message'];
    shortId = json['short_id'];
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['author_email'] = authorEmail;
    data['author_name'] = authorName;
    data['created_at'] = createdAt;
    data['id'] = id;
    data['message'] = message;
    data['short_id'] = shortId;
    data['title'] = title;
    return data;
  }
}
