class Group {
  late int id;
  late String name;
  late String path;
  late String description;
  late String visibility;
  late bool lfsEnabled;
  String? avatarUrl;
  late String webUrl;
  late bool requestAccessEnabled;
  late String fullName;
  late String fullPath;
  int? parentId;

  Group(
      {required this.id,
      required this.name,
      required this.path,
      required this.description,
      required this.visibility,
      required this.lfsEnabled,
      this.avatarUrl,
      required this.webUrl,
      required this.requestAccessEnabled,
      required this.fullName,
      required this.fullPath,
      this.parentId});

  Group.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    path = json['path'];
    description = json['description'];
    visibility = json['visibility'];
    lfsEnabled = json['lfs_enabled'];
    avatarUrl = json['avatar_url'];
    webUrl = json['web_url'];
    requestAccessEnabled = json['request_access_enabled'];
    fullName = json['full_name'];
    fullPath = json['full_path'];
    parentId = json['parent_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['path'] = path;
    data['description'] = description;
    data['visibility'] = visibility;
    data['lfs_enabled'] = lfsEnabled;
    data['avatar_url'] = avatarUrl;
    data['web_url'] = webUrl;
    data['request_access_enabled'] = requestAccessEnabled;
    data['full_name'] = fullName;
    data['full_path'] = fullPath;
    data['parent_id'] = parentId;
    return data;
  }
}
