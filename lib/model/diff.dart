class Diff {
  late String oldPath;
  late String newPath;
  late String aMode;
  late String bMode;
  late bool newFile;
  late bool renamedFile;
  late bool deletedFile;
  late String diff;

  Diff(
      {required this.oldPath,
      required this.newPath,
      required this.aMode,
      required this.bMode,
      required this.newFile,
      required this.renamedFile,
      required this.deletedFile,
      required this.diff});

  Diff.fromJson(Map<String, dynamic> json) {
    oldPath = json['old_path'];
    newPath = json['new_path'];
    aMode = json['a_mode'];
    bMode = json['b_mode'];
    newFile = json['new_file'];
    renamedFile = json['renamed_file'];
    deletedFile = json['deleted_file'];
    diff = json['diff'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['old_path'] = oldPath;
    data['new_path'] = newPath;
    data['a_mode'] = aMode;
    data['b_mode'] = bMode;
    data['new_file'] = newFile;
    data['renamed_file'] = renamedFile;
    data['deleted_file'] = deletedFile;
    data['diff'] = diff;
    return data;
  }
}
