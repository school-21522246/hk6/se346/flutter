class Runner {
  late bool active;
  late String? description;
  late int id;
  late bool isShared;
  late String? ipAddress;
  late String? name;
  late bool online;
  late String? status;

  Runner(
      {required this.active,
      required this.description,
      required this.id,
      required this.isShared,
      required this.ipAddress,
      required this.name,
      required this.online,
      required this.status});

  Runner.fromJson(Map<String, dynamic> json) {
    active = json['active'];
    description = json['description'];
    id = json['id'];
    isShared = json['is_shared'];
    ipAddress = json['ip_address'];
    name = json['name'];
    online = json['online'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['active'] = active;
    data['description'] = description;
    data['id'] = id;
    data['is_shared'] = isShared;
    data['ip_address'] = ipAddress;
    data['name'] = name;
    data['online'] = online;
    data['status'] = status;
    return data;
  }
}
