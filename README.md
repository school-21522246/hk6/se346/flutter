# gitlab_flutter

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

Source-code: https://gitlab.com/school-21522246/hk6/se346/gitlab-flutter

Bước 1: Tạo Personal Access Token (PAT)
Hướng dẫn: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html

Bước 2: Clone source code
Git clone https://gitlab.com/school-21522246/hk6/se346/gitlab-flutter

Bước 3: Chạy code (flutter)
Flutter run
